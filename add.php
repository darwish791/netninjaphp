<?php

include "config/db_connect.php"; 

$email = $title = $ingredients = "";
$errors = ['email' => '', 'title' => '', 'ingredients' => ''];
if(isset($_POST['submit'])) {

	/*check email*/
	if(empty(htmlspecialchars($_POST['email']))){
		$errors['email'] = "email is required<br>";
	}else{
		$email = $_POST['email'];
		if(!filter_var($email, FILTER_VALIDATE_EMAIL)){
			$errors['email'] = "email must be valid email<br>";
		}
	}
	/*End check email*/

	/*check title*/
	if(empty(htmlspecialchars($_POST['title']))){
		$errors['title'] = "title is required<br>";
	}else{
		$title = $_POST['title'];
		if(!preg_match('/^[a-zA-Z\s]+$/', $title)){
			$errors['title'] = "title must be letter and spaces only<br>";
		}
	}
	/*End check title*/

	/*check ingredients*/
	if(empty(htmlspecialchars($_POST['ingredients']))){
			$errors['ingredients'] = "ingredients is required";
	}else{
		$ingredients = $_POST['ingredients'];
		if(!preg_match('/^([a-zA-Z\s]+)(,\s*[a-zA-Z\s]*)*$/', $ingredients)){
			$errors['ingredients'] = "ingredients must be comma separated list only";
		}
	}
	/*End check ingredients*/

	if(array_filter($errors)) { // return true if there are error
		echo "There are error";
	}else{
		$email = mysqli_real_escape_string($connection, $_POST['email']);
		$title = mysqli_real_escape_string($connection, $_POST['title']);
		$ingredients = mysqli_real_escape_string($connection, $_POST['ingredients']);
		
		// Created sql query
		$query = "INSERT INTO pizzas(title, email, ingredients) VALUES ('$title', '$email', '$ingredients')";
		
		// Save to db and check
		if(mysqli_query($connection, $query)){
			// success
			header('Location: index.php'); // Go to index.php
		}else{
			// error
			echo "Connection error: " . mysqli_error($connection);
		}
	}
}
?>
<!DOCTYPE html>
<html lang="en">
<?php include "templates/header.php"; ?>

<section class="container grey-text">
	<h4 class="center">Add a pizza</h4>
	<form action="add.php" class="white" method="post">
		<label for="email">Your Email</label>
		<input type="text" name="email" value="<?php echo htmlspecialchars($email); ?>">
		<div class="red-text"><?php echo $errors['email']; ?></div>
		<label for="email">Pizza Title</label>
		<input type="text" name="title" value="<?php echo htmlspecialchars($title); ?>">
		<div class="red-text"><?php echo $errors['title']; ?></div>
		<label for="email">Ingredients (comma separated):</label>
		<input type="text" name="ingredients" value="<?php echo htmlspecialchars($ingredients); ?>">
		<div class="red-text"><?php echo $errors['ingredients']; ?></div>
		<div class="center">
			<input type="submit" name="submit" value="Submit" class="btn brand z-depth-0">
		</div>
	</form>
</section>



<?php include "templates/footer.php"; ?>  
</html>