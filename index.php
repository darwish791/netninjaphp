<?php

	include "config/db_connect.php";
	//--------------------------------------------------------
	
	// 1. write query for all data
	$query = "SELECT title, ingredients, id FROM pizzas ORDER BY created_at";
	// $query = "SELECT * FROM pizzas"
	
	// 2. make query and get result
	$result = mysqli_query($connection, $query);
	
	// 3. fetch the resulting rows as an array
	$pizzas = mysqli_fetch_all($result, MYSQLI_ASSOC);
	//$pizzas = mysqli_fetch_assoc($result);
	
	// 4. free variable result from memory(not need anymore)
	mysqli_free_result($result);
	
	// 5. Close Connection
	mysqli_close($connection); 
	
	//print_r($pizzas);

	//--------------------------------------------------------

	// print_r(explode(",", $pizzas[0]['ingredients']));
?>


<!DOCTYPE html>
<html lang="en">
<?php include "templates/header.php"; ?>

	<h4 class="center grey-text">Pizzas!</h4>
	<div class="container">
		<div class="row">
			
			<?php foreach ($pizzas as $pizza) : ?>
				
				<div class="col s6 md3">
					<div class="card z-depth-0">
						<div class="card-content center">
							<h6><?php echo htmlspecialchars($pizza['title']); ?></h6>							
							<ul>
								<?php foreach(explode(',', $pizza['ingredients']) as $ing) : ?>
								  <li><?php echo htmlspecialchars($ing); ?></li>
								<?php endforeach; ?>
							</ul>
						</div>
						<div class="card-action right-align">
							<a href="#" class="brand-text">more info</a>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
  

    
	


<?php include "templates/footer.php"; ?>  
</html>